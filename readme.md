https://github.com/MicrosoftLearning/mslearn-openai/blob/main/Instructions/Exercises/02-natural-language-azure-openai.md

# pip install
Here’s a list of the packages that are typically necessary for working with AI, particularly with libraries like OpenAI, data processing, and web frameworks:

1. **openai==1.13.3** - Essential for interacting with OpenAI's APIs.
2. **Flask==3.0.1** - Useful for building web applications that can serve AI models.
3. **pydantic==2.9.2** - Useful for data validation and settings management, especially with models.
4. **requests==2.31.0** - Commonly used for making HTTP requests, including to AI APIs.
5. **python-dotenv==0.21.1** - Helpful for managing environment variables, particularly API keys.
6. **httpx==0.27.2** - An alternative to requests for making asynchronous HTTP calls, which can be useful for AI applications.

The other packages listed are not strictly required for basic AI applications, but they may be helpful in specific contexts or for additional functionalities. Let me know if you need a more detailed breakdown!

```
openai==1.13.3
Flask==3.0.1
pydantic==2.9.2
requests==2.31.0
python-dotenv==0.21.1
httpx==0.27.2
```
